//This code is the intellectual property of:
//    NICHOLAS J EGGLESTON
//Created for the Text Adventure Framework Project
//Started on 25 April 2016
//Filename: location.h

#ifndef LOCATION_H
#define LOCATION_H

class Location {
  string shortName;
  string description;
  Location *north, *south, *east, *west, *up, *down;
  Object *stuffInRoom;

  Location* goNorth();
  Location* goSouth();
  Location* goEast();
  Location* goWest();
  Location* goUp();
  Location* goDown();

};

#endif